package coursera.course.project.assignment3.jsonweather;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

/**
 * Parses the Json weather data returned from the Weather Services API
 * and returns a List of JsonWeather objects that contain this data.
 */
public class WeatherJSONParser {
    /**
     * Used for logging purposes.
     */
    private final String TAG =
        this.getClass().getCanonicalName();

    /**
     * Parse the @a inputStream and convert it into a List of JsonWeather
     * objects.
     */
    public List<JsonWeather> parseJsonStream(InputStream inputStream)
        throws IOException {
        // TODO -- you fill in here.
        // Create a JsonReader for the inputStream
        try (JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"))) {

            List<JsonWeather> jsonWeathers = new ArrayList<JsonWeather>();

            // First Assume only request one weather info
            JsonWeather jsonWeather = parseJsonStreamSingle(reader);
            if (jsonWeather != null) {
                jsonWeathers.add(jsonWeather);
            }
            return jsonWeathers;
        }
    }

    /**
     * Parse a single Json stream and convert it into a JsonWeather
     * object.
     */
    public JsonWeather parseJsonStreamSingle(JsonReader reader)
        throws IOException {
        // TODO -- you fill in here.
        return parseJsonWeather(reader);
    }

    /**
     * Parse a Json stream and convert it into a List of JsonWeather
     * objects.
     */
    public List<JsonWeather> parseJsonWeatherArray(JsonReader reader)
        throws IOException {

        // TODO -- you fill in here.
        return null;
    }

    /**
     * Parse a Json stream and return a JsonWeather object.
     */
    public JsonWeather parseJsonWeather(JsonReader reader) 
        throws IOException {

        // Parse a single weather Json stream
        // ignore "coord" and "visibility"
        reader.beginObject();

        JsonWeather jsonWeather = new JsonWeather();
        try {
            while (reader.hasNext()) {
                String name = reader.nextName();
                switch (name) {
                    case JsonWeather.sys_JSON:
                        Log.d(TAG, "parsing sys");
                        jsonWeather.setSys(parseSys(reader));
                        break;
                    case JsonWeather.weather_JSON:
                        Log.d(TAG, "parsing weather");
                        jsonWeather.setWeather(parseWeathers(reader));
                        break;
                    case JsonWeather.base_JSON:
                        Log.d(TAG, "parsing base");
                        jsonWeather.setBase(reader.nextString());
                        break;
                    case JsonWeather.main_JSON:
                        Log.d(TAG, "parsing main");
                        jsonWeather.setMain(parseMain(reader));
                        break;
                    case JsonWeather.wind_JSON:
                        Log.d(TAG, "parsing wind");
                        jsonWeather.setWind(parseWind(reader));
                        break;
                    case JsonWeather.dt_JSON:
                        Log.d(TAG, "parsing dt");
                        jsonWeather.setDt(reader.nextLong());
                        break;
                    case JsonWeather.id_JSON:
                        Log.d(TAG, "parsing id");
                        jsonWeather.setId(reader.nextLong());
                        break;
                    case JsonWeather.name_JSON:
                        Log.d(TAG, "parsing name");
                        jsonWeather.setName(reader.nextString());
                        break;
                    case JsonWeather.cod_JSON:
                        Log.d(TAG, "parsing cod");
                        jsonWeather.setCod(reader.nextLong());
                        break;
                    case JsonWeather.message_JSON: // in this case city not found
                        String message = reader.nextString();
                        Log.d(TAG, "parsing an error message: " + message);
                        jsonWeather.setMessage(message);
                        break;
                    default:
                        Log.d(TAG, "skip name " + name);
                        reader.skipValue();
                }
            }
        } finally {
            reader.endObject();
        }
        return jsonWeather;
    }
    
    /**
     * Parse a Json stream and return a List of Weather objects.
     * We might have multiple Weather in a time
     */
    public List<Weather> parseWeathers(JsonReader reader) throws IOException {
        reader.beginArray();
        List<Weather> weathers = new ArrayList<Weather>();
        try {
            while (reader.hasNext()) {
                weathers.add(parseWeather(reader));
            }
        } finally {
            reader.endArray();
        }
        return weathers;
    }

    /**
     * Parse a Json stream and return a Weather object.
     */
    public Weather parseWeather(JsonReader reader) throws IOException {
        reader.beginObject();
        Weather weather = new Weather();
        try {
            while (reader.hasNext()) {
                String name = reader.nextName();
                switch (name) {
                    case Weather.id_JSON:
                        weather.setId(reader.nextLong());
                        break;
                    case Weather.main_JSON:
                        weather.setMain(reader.nextString());
                        break;
                    case Weather.description_JSON:
                        weather.setDescription(reader.nextString());
                        break;
                    case Weather.icon_JSON:
                        weather.setIcon(reader.nextString());
                        break;
                    default: // should not happen
                        reader.skipValue();
                        break;
                }
            }
        } finally {
            reader.endObject();
        }
        return weather;
    }
    
    /**
     * Parse a Json stream and return a Main Object.
     */
    public Main parseMain(JsonReader reader) 
        throws IOException {
        reader.beginObject();
        Main main = new Main();
        try {
            while (reader.hasNext()) {
                String name = reader.nextName();
                switch (name) {
                    case Main.temp_JSON:
                        main.setTemp(reader.nextDouble());
                        break;
                    case Main.tempMin_JSON:
                        main.setTempMin(reader.nextDouble());
                        break;
                    case Main.tempMax_JSON:
                        main.setTempMax(reader.nextDouble());
                        break;
                    case Main.pressure_JSON:
                        main.setPressure(reader.nextDouble());
                        break;
                    case Main.seaLevel_JSON:
                        main.setSeaLevel(reader.nextDouble());
                        break;
                    case Main.grndLevel_JSON:
                        main.setGrndLevel(reader.nextDouble());
                        break;
                    case Main.humidity_JSON:
                        main.setHumidity(reader.nextLong());
                        break;
                    default:
                        reader.skipValue();
                        break;
                }
            }
        } finally {
            reader.endObject();
        }
        return main;
    }

    /**
     * Parse a Json stream and return a Wind Object.
     */
    public Wind parseWind(JsonReader reader) throws IOException {
        reader.beginObject();
        Wind wind = new Wind();
        try {
            while (reader.hasNext()) {
                String name = reader.nextName();
                switch (name) {
                    case Wind.speed_JSON:
                        wind.setSpeed(reader.nextDouble());
                        break;
                    case Wind.deg_JSON:
                        wind.setDeg(reader.nextDouble());
                        break;
                    default:
                        reader.skipValue();
                        break;
                }
            }
        } finally {
            reader.endObject();
        }
        return wind;
    }

    /**
     * Parse a Json stream and return a Sys Object.
     */
    public Sys parseSys(JsonReader reader) throws IOException{
        reader.beginObject();
        Sys sys = new Sys();
        try {
            while (reader.hasNext()) {
                String name = reader.nextName();
                switch (name) {
                    case Sys.message_JSON:
                        sys.setMessage(reader.nextDouble());
                        break;
                    case Sys.country_JSON:
                        sys.setCountry(reader.nextString());
                        break;
                    case Sys.sunrise_JSON:
                        sys.setSunrise(reader.nextLong());
                        break;
                    case Sys.sunset_JSON:
                        sys.setSunset(reader.nextLong());
                        break;
                    default:
                        reader.skipValue();
                        break;
                }
            }
        } finally {
            reader.endObject();
        }
        return sys;
    }
}
