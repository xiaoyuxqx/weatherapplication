package coursera.course.project.assignment3.operations;

import android.view.View;

import coursera.course.project.assignment3.activities.WeatherActivity;

/**
 * Created by Yu Xiao on 2015/5/30.
 * This class declares all the weather-related operations
 * Inherits the style from vandy.mooc
 */
public interface WeatherOps {
    /**
     * Initiate the service binding protocol
     */
    public void bindService();

    /**
     * Initiate the service unbinding protocol
     */
    public void unbindService();

    /**
     * Initiate the synchronous weather query when the user presses
     * the "Query Sync" button
     */
    public void weatherQuerySync(View v);

    /**
     * Initiate the asynchronous weather query when the user presses
     * the "Query Async" button
     */
    public void weatherQueryAsync(View v);

    /**
     * Called after a runtime configuration change occurs to finish
     * the initialization steps
     */
    public void onConfigurationChange(WeatherActivity activity);
}
