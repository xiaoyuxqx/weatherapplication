package coursera.course.project.assignment3.services;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import coursera.course.project.assignment3.aidl.WeatherCall;
import coursera.course.project.assignment3.aidl.WeatherData;
import coursera.course.project.assignment3.utils.Utils;

/**
 * Created by Yu Xiao on 2015/5/31.
 * Inherits from vandy.mooc
 */
public class WeatherServiceSync extends LifecycleLoggingService{
    /**
     * Factory method that makes an Intent used to start the
     * WeatherServiceSync when passed to bindService()
     */
    public static Intent makeIntent(Context context) {
        return new Intent(context, WeatherServiceSync.class);
    }

    /**
     * Called when a client (e.g., WeatherActivity) calls
     * bindService() with the proper Intent. Returns the
     * implementation of WeatherCall, which is implicitly ast as an IBinder
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mWeatherCallImpl;
    }

    /**
     * The concrete implementation of the AIDL Interface WeatherCall,
     * which extends the Stub class that implements WeatherCall,
     * thereby allowing Android to handle calls across process boundaries.
     * This method runs in a separate Thread as part of the Android Binder Framework
     */
    WeatherCall.Stub mWeatherCallImpl = new WeatherCall.Stub() {
        /**
         * Implement the AIDL WeatherCall getCurrentWeather() method,
         * which forwards to forwards to TODO DownloadUtils getResults()
         * to obtain the results from the Weather Web service and then
         * return the results back to the Activity.
         * @param Weather
         * @return
         * @throws RemoteException
         */
        @Override
        public List<WeatherData> getCurrentWeather(String Weather) throws RemoteException {
            // Call the Weather Web service to get the list
            // possible weather result
            List<WeatherData> weatherResults = Utils.getResults(Weather);

            if (weatherResults != null && weatherResults.size() > 0) {
                Log.d(TAG, "" + weatherResults.size() + " results for weather: " + Weather);

                // Return the list of weather back to the WeatherActivity
                return weatherResults;
            } else {
                weatherResults = new ArrayList<WeatherData>();
                return weatherResults;
            }

        }
    };

}
