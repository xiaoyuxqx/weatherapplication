package coursera.course.project.assignment3.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import java.util.List;

import coursera.course.project.assignment3.aidl.WeatherData;
import coursera.course.project.assignment3.aidl.WeatherRequest;
import coursera.course.project.assignment3.aidl.WeatherResults;
import coursera.course.project.assignment3.utils.Utils;

public class WeatherServiceAsync extends LifecycleLoggingService {

    /**
     * Factory method that makes an Intent used to start
     * the WeatherServiceAsync when passed to bindService()
     * @param context
     *      The context of the calling component
     * @return
     *      The intent for binding to service
     */
    public static Intent makeIntent(Context context) {
        return new Intent(context, WeatherServiceAsync.class);
    }



    @Override
    /**
     * Called when a client (e.g., WeatherActivity) calls
     * bindService() with the proper Intent.  Returns the
     * implementation of WeatherRequest, which is implicitly cast as
     * an IBinder.
     */
    public IBinder onBind(Intent intent) {
        return mWeatherRequestImpl;
    }

    /**
     * The concrete implementation of the AIDL Interface
     * WeatherRequest, which extends the Stub class that implements
     * WeatherRequest, thereby allowing Android to handle calls across
     * process boundaries.  This method runs in a separate Thread as
     * part of the Android Binder framework.
     *
     * This implementation plays the role of Invoker in the Broker
     * Pattern.
     */
    WeatherRequest.Stub mWeatherRequestImpl = new WeatherRequest.Stub() {
        /**
         * Implement the AIDL WeatherRequest getCurrentWeather()
         * method, which forwards to DownloadUtils getResults() to
         * obtain the results from the Weather Web service and
         * then sends the results back to the Activity via a
         * callback.
         */
        @Override
        public void getCurrentWeather(String Weather, WeatherResults results) throws RemoteException {
            // Call the Weather Web service to get the list of
            // possible weather information
            List<WeatherData> weatherResults = Utils.getResults(Weather);

            // Invoke a one-way callback to send list of Weather info
            // back to the WeatherActivity
            if (weatherResults != null) {
                Log.d(TAG, "" + weatherResults.size()
                        + " results for Weather: "
                        + Weather);
            } else {
                Log.d(TAG, "weatherResults is null");
            }
            results.sendResults(weatherResults);
        }
    };
}
