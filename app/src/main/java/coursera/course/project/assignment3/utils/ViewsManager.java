package coursera.course.project.assignment3.utils;

import android.app.Activity;
import android.nfc.Tag;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.HashMap;

import coursera.course.project.assignment3.R;
import coursera.course.project.assignment3.aidl.WeatherData;
import coursera.course.project.assignment3.jsonweather.Weather;

/**
 * Created by Yu Xiao on 2015/6/2.
 * This class is used to manager all views using in this app
 * All the views related to weather information
 */
public class ViewsManager {
    /**
     * Tag for Debugging
     */
    protected final String TAG = getClass().getSimpleName();

    // Some special Symbol
    private static final String sCELSIUS = "\u2103";
    private static final String sPERCENT = "%";
    private static final String sWIND_SPEED_UNIT = "m/s";

    // Some TAG for TextViews
    private static final String sCITY_NAME  = "CITY_NAME";
    private static final String sTEMP       = "TEMP";
    private static final String sTEMP_MIN   = "TEMP_MIN";
    private static final String sTEMP_MAX   = "TEMP_MAX";
    private static final String sHUMIDITY   = "HUMIDITY";
    private static final String sWIND_SPEED  = "WIND_SPEED";
    private static final String sSUNRISE    = "SUNRISE";
    private static final String sSUNSET     = "SUNSET";
    private static final String sDESCRIPTION = "DESCRIPTION";
    // A hash map for id, reference of TextViews
    private HashMap<String, TextViewPair> textViewPairHashMap;

    // City entered by the user
    protected WeakReference<EditText> mCityEditText;

    /**
     * Constructor
     * @param activity
     *      current application's activity
     */
    public ViewsManager(Activity activity) {
        // Store all references
        mCityEditText = new WeakReference<>
                ((EditText)activity.findViewById(R.id.city_editText));
        textViewPairHashMap = new HashMap<>();
        textViewPairHashMap.put(sCITY_NAME,
                new TextViewPair(activity, R.id.cityName_textView));
        textViewPairHashMap.put(sTEMP,
                new TextViewPair(activity, R.id.temp_textView));
        textViewPairHashMap.put(sTEMP_MIN,
                new TextViewPair(activity, R.id.minTemp_textView));
        textViewPairHashMap.put(sTEMP_MAX,
                new TextViewPair(activity, R.id.maxTemp_textView));
        textViewPairHashMap.put(sHUMIDITY,
                new TextViewPair(activity, R.id.humidity_textView));
        textViewPairHashMap.put(sWIND_SPEED,
                new TextViewPair(activity, R.id.windSpeed_textView));
        textViewPairHashMap.put(sSUNRISE,
                new TextViewPair(activity, R.id.sunrise_textView));
        textViewPairHashMap.put(sSUNSET,
                new TextViewPair(activity, R.id.sunset_textView));
        textViewPairHashMap.put(sDESCRIPTION,
                new TextViewPair(activity, R.id.description_textView));

    }

    /**
     * Get the City Name user inputs
     * @return
     */
    public String getCityName() {
        return mCityEditText.get().getText().toString();
    }

    public void updateViewFields(WeatherData weather) {
        textViewPairHashMap.get(sCITY_NAME).setText(weather.getName());
        textViewPairHashMap.get(sTEMP).setText("" + (int) weather.getTemp() + sCELSIUS);
        textViewPairHashMap.get(sTEMP_MAX).setText("" + (int) weather.getTempMax() + sCELSIUS);
        textViewPairHashMap.get(sTEMP_MIN).setText("" + (int) weather.getTempMin() + sCELSIUS);
        textViewPairHashMap.get(sHUMIDITY).setText("" + weather.getHumidity() + sPERCENT);
        textViewPairHashMap.get(sWIND_SPEED).setText("" + weather.getSpeed() + sWIND_SPEED_UNIT);
        textViewPairHashMap.get(sSUNRISE).setText(Utils.timeStampToString(weather.getSunrise()));
        textViewPairHashMap.get(sSUNSET).setText(Utils.timeStampToString(weather.getSunSet()));
        textViewPairHashMap.get(sDESCRIPTION).setText(weather.getDescription());
    }

    /**
     * This private class helps to store the Reference and ID of TextView
     */
    private class TextViewPair {
        public int id;
        public WeakReference<TextView> reference;

        public TextViewPair(Activity activity, int id) {
            this.id = id;
            reference = new WeakReference<>((TextView)activity.findViewById(id));
        }

        public void setText(String string) {
            reference.get().setText(string);
        }
    }

}
