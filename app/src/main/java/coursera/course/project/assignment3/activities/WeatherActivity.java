package coursera.course.project.assignment3.activities;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import coursera.course.project.assignment3.R;
import coursera.course.project.assignment3.operations.WeatherOps;
import coursera.course.project.assignment3.operations.WeatherOpsIml;
import coursera.course.project.assignment3.utils.RetainedFragmentManager;

public class WeatherActivity extends LifecycleLoggingActivity {

    /**
     * Used to retain the WeatherOps state between runtime configuration
     * changes
     */
    protected final RetainedFragmentManager mRetainedFragmentManager =
            new RetainedFragmentManager(this.getFragmentManager(), TAG);

    private WeatherOps mWeatherOps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Create WeatherOps object one time
        mWeatherOps = new WeatherOpsIml(this);

        // Handle any configuration change
        handleConfigurationChanges();

    }

    @Override
    protected void onDestroy() {
        // Unbind from the Service
        mWeatherOps.unbindService();

        super.onDestroy();
    }

    protected void handleConfigurationChanges() {
        // If this method returns true then this is the first time
        // the activity has been created
        if (mRetainedFragmentManager.firstTimeIn()) {
            Log.d(TAG, "First time onCreate() call");

            // Create the WeatherOps object one time.
            mWeatherOps = new WeatherOpsIml(this);

            // Store the WeatherOps into the RetainedFragmentManager
            mRetainedFragmentManager.put("WEATHER_OPS_STATE", mWeatherOps);

            // Initiate the service binding protocol
            mWeatherOps.bindService();
        } else {
            // The RetainedFragmentManager was previously initialized,
            // which means that a runtime configuration change occurred
            Log.d(TAG, "Second or subsequent onCreate() call");

            // Obtain the mWeatherOps from the RetainedFragmentManager
            mWeatherOps = mRetainedFragmentManager.get("WEATHER_OPS_STATE");

            // This check shouldn't be necessary under normal circumstances
            // but it's better to lose state than to crash!
            if (mWeatherOps == null) {
                // Create the WeatherOps again
                mWeatherOps = new WeatherOpsIml(this);

                // Store the WeatherOps into the RetainedFragmentManager again
                mRetainedFragmentManager.put("WEATHER_OPS_STATE", mWeatherOps);

                // Initiate the service binding protocol again
                mWeatherOps.bindService();
            } else {
                // Inform it that the runtime configuration change has
                // completed
                mWeatherOps.onConfigurationChange(this);
            }
        }
    }

    /**
     * Initiate the synchronous weather query when user presses the "Query Sync"
     * button
     */
    public void weatherQuerySync(View v) {
        mWeatherOps.weatherQuerySync(v);
    }

    /**
     * Initiate the synchronous weather query when user presses the "Query Async"
     * button
     */
    public void weatherQueryAsync(View v) {
        mWeatherOps.weatherQueryAsync(v);
    }


}
