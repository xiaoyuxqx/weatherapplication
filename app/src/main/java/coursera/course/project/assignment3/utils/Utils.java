package coursera.course.project.assignment3.utils;

import android.content.Context;
import android.nfc.Tag;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import coursera.course.project.assignment3.aidl.WeatherData;
import coursera.course.project.assignment3.jsonweather.JsonWeather;
import coursera.course.project.assignment3.jsonweather.Sys;
import coursera.course.project.assignment3.jsonweather.Weather;
import coursera.course.project.assignment3.jsonweather.WeatherJSONParser;

/**
 * Created by Yu Xiao on 2015/5/31.
 * Inspired by vandy.mooc
 */
public class Utils {
    /**
     * Used for logging purposes.
     */
    private final static String TAG = Utils.class.getCanonicalName();

    /**
     * URL of Weather Website API
     */
    private final static String sWeather_Web_Service_URL =
            "http://api.openweathermap.org/data/2.5/weather?q=";
    private final static String sCelsius_Unit = "&units=metric";

    /**
     * Core concrete routine for getting result from Weather Website
     * This method will be called by services who execute the query
     * @param Weather
     *      URL of Request
     * @return
     *      List of WeatherData which converted from JsonWeatherData
     */
    public static List<WeatherData> getResults(final String Weather) {
        // Create a List that will return the WeatherData obtained
        // from the Weather Service web service
        final List<WeatherData> returnList = new ArrayList<WeatherData>();

        // A List of JsonWeather objects
        List<JsonWeather> jsonWeathers = null;

        try {
            // Append the location to create the full URL
            final URL url = new URL(sWeather_Web_Service_URL + Weather + sCelsius_Unit);

            // Opens a connection to the Weather Service.
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            // Sends the GET request and reads the Json results
            try (InputStream in = new BufferedInputStream(urlConnection.getInputStream())) {
                // Create the parser
                final WeatherJSONParser parser = new WeatherJSONParser();

                // Parse the Json results and create JsonWeather data objects
                jsonWeathers = parser.parseJsonStream(in);
            } finally {
                urlConnection.disconnect();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (jsonWeathers != null && jsonWeathers.size() > 0) {
            // Convert the JsonWeather data objects to out WeatherData
            // object, which can be passed between process
            for (JsonWeather jsonWeather : jsonWeathers) {
                if (jsonWeather.getMessage() != null) continue;
                returnList.add(new WeatherData(jsonWeather.getName(),
                        jsonWeather.getWind().getSpeed(),
                        jsonWeather.getWind().getDeg(),
                        jsonWeather.getMain().getTemp(),
                        jsonWeather.getMain().getTempMin(),
                        jsonWeather.getMain().getTempMax(),
                        jsonWeather.getMain().getHumidity(),
                        jsonWeather.getSys().getSunrise(),
                        jsonWeather.getSys().getSunset(),
                        jsonWeather.getWeather().get(0).getDescription(),
                        jsonWeather.getDt()));
            }
            Log.d(TAG, "receive valid Json Weather Info:" + returnList);
            // Return the List of WeatherData
            return returnList;
        } else {
            return null;
        }


    }

    /**
     * Show a toast message to user
     * @param context
     *      The context to show toast
     * @param message
     *      The content to show
     */
    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static boolean isOutOfDate(long timeStamp, final long TIMEOUT) {
        return Math.abs(timeStamp - System.currentTimeMillis()) > TIMEOUT;
    }

    /**
     * Change returned time stamp to "HH:MM" format String
     * @param timeStamp
     *      returned time stamp
     * @return
     *      formatted time string
     */
    public static String timeStampToString(long timeStamp) {
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(timeStamp * 1000);

        return appendTwoDigits(date.get(Calendar.HOUR))
                + ":"
                + appendTwoDigits(date.get(Calendar.MINUTE));
    }

    /**
     * padding zero for timeStampToString method
     * @param n
     *      digit might need to be padded zero
     * @return
     *      two digits
     */
    private static String appendTwoDigits(int n) {
        return n < 10 ? "0" + n : "" + n;
    }

    /**
     * Ensure this class is only used as a utility.
     */
    private Utils() {
        throw new AssertionError();
    }
}
