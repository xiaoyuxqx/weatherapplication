package coursera.course.project.assignment3.operations;

import android.content.Context;
import android.os.AsyncTask;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.List;

import coursera.course.project.assignment3.R;
import coursera.course.project.assignment3.activities.WeatherActivity;
import coursera.course.project.assignment3.aidl.WeatherCall;
import coursera.course.project.assignment3.aidl.WeatherData;
import coursera.course.project.assignment3.aidl.WeatherRequest;
import coursera.course.project.assignment3.aidl.WeatherResults;
import coursera.course.project.assignment3.jsonweather.Weather;
import coursera.course.project.assignment3.services.WeatherServiceAsync;
import coursera.course.project.assignment3.utils.Utils;
import coursera.course.project.assignment3.services.WeatherServiceSync;
import coursera.course.project.assignment3.utils.GenericServiceConnection;
import coursera.course.project.assignment3.utils.ViewsManager;

/**
 * Created by Yu Xiao on 2015/5/30.
 * This class implements all the weather-related operations defined in the WeatherOps interface
 * Inherits from vandy.mooc
 */
public class WeatherOpsIml implements WeatherOps{
    /**
     * Debugging tag used by the Android logger
     */
    protected final String TAG = getClass().getSimpleName();

    /**
     * Used to enable garbage collection
     */
    protected WeakReference<WeatherActivity> mWeatherActivity;

    /**
     * Define WeakReference of UI here
     */
    // TextViews for temperature display
    ViewsManager mViewsManager = null;

    /**
     * Define result for display here
     */
    protected List<WeatherData> mResults;

    /**
     * This GenericServiceConnection is used to receive results after
     * binding to the WeatherServiceSync Service using bindService()
     */
    private GenericServiceConnection<WeatherCall> mServiceConnectionSync;

    /**
     * This Generic ServiceConnection is used to receive results after
     * binding to the WeatherAsync Service using bindService()
     */
    private GenericServiceConnection<WeatherRequest> mServiceConnectionAsync;

    // the time stamp for tracking last query time.
    private long lastQueryTimeStamp = 0;

    // Time out in milliseconds
    // 1000 millisecond/sec * 5 sec
    private static long TIMEOUT = 1000 * 5;

    // Last query city
    private String lastQueryCityName;


    /**
     * Constructor initializes the fields
     */
    public WeatherOpsIml(WeatherActivity activity) {
        // Initialize the WeakReference
        mWeatherActivity = new WeakReference<>(activity);

        // Finish the initialization steps
        initializeViewFields();
        initializeNonViewFields();
    }

    /**
     * Initialize the View fields, which are all stored as
     * WeakReferences to enable garbage collection
     */
    private void initializeViewFields() {
        // Get references to the UI components
        mWeatherActivity.get().setContentView(R.layout.activity_main);

        // Store the EditText that holds the city name entered by the user
        mViewsManager = new ViewsManager(mWeatherActivity.get());

        // Display results if any (due to runtime configuration change).
        if (mResults != null) {
            displayResults(mResults);
        }
    }

    /**
     * (Re)initialize the non-view fields (e.g.,
     * GenericServiceConnection objects)
     */
    private void initializeNonViewFields() {
        mServiceConnectionSync =
                new GenericServiceConnection<WeatherCall>(WeatherCall.class);

        mServiceConnectionAsync =
                new GenericServiceConnection<WeatherRequest>(WeatherRequest.class);
    }

    @Override
    public void bindService() {
        Log.d(TAG, "calling bindService()");

        // Launch the Weather Bound Services if they aren't already
        // running via a call to bindService(), which binds this
        // activity to the WeatherService* if they aren't already
        // bound
        if (mServiceConnectionSync.getInterface() == null) {
            mWeatherActivity.get().getApplicationContext().bindService(
                    WeatherServiceSync.makeIntent(mWeatherActivity.get()),
                    mServiceConnectionSync,
                    Context.BIND_AUTO_CREATE
            );
        }

        if (mServiceConnectionAsync.getInterface() == null) {
            mWeatherActivity.get().getApplicationContext().bindService(
                    WeatherServiceAsync.makeIntent(mWeatherActivity.get()),
                    mServiceConnectionAsync,
                    Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    public void unbindService() {
        if (mWeatherActivity.get().isChangingConfigurations()) {
            Log.d(TAG, "just a configuration change - unbindService() not called");
            return;
        } else {
            Log.d(TAG, "calling unbindService()");
        }

        // Unbind the Sync Service
        if (mServiceConnectionSync.getInterface() != null) {
            mWeatherActivity.get().getApplicationContext().unbindService(mServiceConnectionSync);
        }

        if (mServiceConnectionAsync.getInterface() != null) {
            mWeatherActivity.get().getApplicationContext().unbindService(mServiceConnectionAsync);
        }
    }

    /**
     * Called after a runtime configuration change occurs to finish
     * the initialization steps
     * @param activity
     */
    @Override
    public void onConfigurationChange(WeatherActivity activity) {
        Log.d(TAG, "onConfigurationChange() called");

        // Reset the mWeatherActivity WeakReference
        mWeatherActivity = new WeakReference<>(activity);

        // (Re)initialize all the View fields
        initializeViewFields();
    }

    @Override
    public void weatherQuerySync(View v) {
        // Check if need to query from Web
        if (doesGiveUpQuery()) {
            Utils.showToast(mWeatherActivity.get(), "Query too fast");
            return;
        } else { // update query time
            lastQueryTimeStamp = System.currentTimeMillis();
            lastQueryCityName = mViewsManager.getCityName();
        }

        final WeatherCall weatherCall = mServiceConnectionSync.getInterface();

        if (weatherCall != null) {
            // Get the City entered by the user
            //final String cityName = mCityEditText.get().getText().toString();
            final String cityName = mViewsManager.getCityName();

            // Use an anonymous AsyncTask to download Weather data in a seperate
            // Thread and then display any results in the UI thread
            new AsyncTask<String, Void, List<WeatherData>>() {
                private String cityName;

                @Override
                protected List<WeatherData> doInBackground(String... cityNames) {
                    try {
                        cityName = cityNames[0];
                        return weatherCall.getCurrentWeather(cityName);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(List<WeatherData> weatherDatas) {
                    if (weatherDatas.size() > 0) {
                        Utils.showToast(mWeatherActivity.get(),
                                        "Weather updated Successfully");
                        displayResults(weatherDatas);
                    }
                    else
                        // Toast a failure message
                        Utils.showToast(mWeatherActivity.get(),
                                        "no weather info for " + cityName + " found");
                }
                // Execute the AsyncTask to expand the acronym without
                // block the caller
            }.execute(cityName);
        }

    }

    /**
     * Initiate the asynchronous weather query when user presses
     * the "Query Async" button
     */
    @Override
    public void weatherQueryAsync(View v) {
        // Check if need to query from Web
        if (doesGiveUpQuery()) {
            Utils.showToast(mWeatherActivity.get(), "Query too fast");
            return;
        } else { // update query time
            lastQueryTimeStamp = System.currentTimeMillis();
            lastQueryCityName = mViewsManager.getCityName();
        }

        WeatherRequest weatherRequest = mServiceConnectionAsync.getInterface();

        if (weatherRequest != null) {
            // Get the city name entered by the user
            //final String cityName = mCityEditText.get().getText().toString();
            final String cityName = mViewsManager.getCityName();

            try {
                // Invoke a one-way AIDL call, which does not block
                // the client. The results are returned via the
                // sendResults() methods of the mWeatherResults callback object,
                // which runs in a Thread from the Thread pool managed by
                // the Binder framework
                weatherRequest.getCurrentWeather(cityName, mWeatherResults);
            } catch (RemoteException e) {
                Log.e(TAG, "RemoteException:" + e.getMessage());
            }
        } else  {
            Log.d(TAG, "weatherRequest was null.");
        }
    }

    private WeatherResults.Stub mWeatherResults = new WeatherResults.Stub() {
        /**
         * This method is invoked by the WeatherServiceAsync to return the results
         * back to the WeatherActivity.
         * @param results
         *      results downloaded from Weather website
         * @throws RemoteException
         */
        @Override
        public void sendResults(final List<WeatherData> results) throws RemoteException {
            // Since the Android Binder framework dispatches this method in a
            // background Thread we need to explicitly post a runnable containing
            // the results to the UI Thread, where it's displayed
            mWeatherActivity.get().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (results.size() > 0) {
                        Utils.showToast(mWeatherActivity.get(),
                                "Weather updated Successfully");
                    } else
                        Utils.showToast(mWeatherActivity.get(),
                                "no weather info found");
                }
            });
        }
    };

    private void displayResults(List<WeatherData> results) {
        // display the returned results to views
        mResults = results;
        for (WeatherData result : mResults) {
            displayResult(result);
        }
    }

    private void displayResult(WeatherData result) {
        // display the returned result
        mViewsManager.updateViewFields(result);
    }

    private boolean doesGiveUpQuery () {
        return !Utils.isOutOfDate(lastQueryTimeStamp, TIMEOUT) &&
                lastQueryCityName.equals(mViewsManager.getCityName());
    }


}
